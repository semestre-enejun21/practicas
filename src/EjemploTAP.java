import java.awt.BorderLayout;
import javax.swing.*;

public class EjemploTAP {

 public static void main(String[] args) {
EjemploTAP objEjemplo = new EjemploTAP();
objEjemplo.run();
}
public void run(){
JFrame ventana = new JFrame("Mi primer programa");
ventana.setLayout(new BorderLayout());
ventana.setSize(300,200);
ventana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
JLabel etiqueta = new JLabel("Hola mundo!");
JButton boton = new JButton("Cerrar");
ventana.add(etiqueta, BorderLayout.CENTER);
ventana.add(boton, BorderLayout.SOUTH);
ventana.setVisible(true);
}
}